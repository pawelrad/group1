# Konfiguracja

1. Uruchom klienta putty i skonfiguruj go jak poniżej

![putty1.png](putty1.png)

![putty2.png](putty2.png)

![putty3.png](putty3.png)

![putty4.png](putty4.png)


***Wersja dla Linuxa:***

```
ssh -D 8888 vagrant@192.168.56.5

```

2. Zaloguj się do serwera ssh


![putty5.png](putty5.png)

Po zestawieniu połączenia zostanie uruchomiony dodatkowy port 8888 na komputerze inicjującego połączenie

![putty6.png](putty6.png)


3. Na serwerze ssh będzie widoczne zestawione połączenie

![server_ssh1.png](server_ssh1.png)

4. Skonfiguruj ustawienia proxy w przeglądarce Mozilla Firefox jak poniżej

![browser.png](browser.png)



# Weryfikacja

Otwórz dowolną stronę w skonfigurowanej przeglądarce

Wykonaj komendę weryfikującą połączenia na komputerze inicjującym połączenie

```

[sda_teacher@kasztanek-OWL ~]$ netstat -antpul | grep firefox
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
tcp        0      0 127.0.0.1:38182         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:33384         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38344         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52216         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52160         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52278         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:33424         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52220         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38276         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38306         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38038         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38208         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52236         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38316         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38036         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:33386         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38172         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52178         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38030         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52306         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38332         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38136         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:33402         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52190         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52196         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52368         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38216         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:33444         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52252         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:38286         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52288         127.0.0.1:8888          ESTABLISHED 44390/firefox       
tcp        0      0 127.0.0.1:52292         127.0.0.1:8888          ESTABLISHED 44390/firefox       
[sda_teacher@kasztanek-OWL ~]$ 

````

Wykonaj komendę weryfikującą połączenia na serwerze ssh

```
──(root㉿kali)-[~]
└─# netstat -antpul | grep ssh
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      4948/sshd: /usr/sbi 
tcp        0      0 10.0.2.15:52230         104.18.20.44:443        ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:41232         104.22.19.233:443       ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:49960         92.123.189.24:443       ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 192.168.56.5:22         192.168.56.1:48461      ESTABLISHED 5279/sshd: vagrant  
tcp        0      0 10.0.2.15:36724         91.134.222.90:443       ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:59894         193.0.19.228:443        ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:49966         92.123.189.24:443       ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:35562         52.35.167.249:443       ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:22            10.0.2.2:44152          ESTABLISHED 3126/sshd: vagrant  
tcp        0      0 10.0.2.15:54962         93.184.220.29:80        ESTABLISHED 5285/sshd: vagrant@ 
tcp        0      0 10.0.2.15:33144         193.0.11.17:443         ESTABLISHED 5285/sshd: vagrant@ 
tcp6       0      0 :::22                   :::*                    LISTEN      4948/sshd: /usr/sbi 
                                                                                                                                                                                             
┌──(root㉿kali)-[~]

```
